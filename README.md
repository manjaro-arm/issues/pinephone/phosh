Welcome to our Bug-Tracker for Manjaro with Phosh
----------------------------------------------------

You may want to browse already [reported issues](https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/phosh/-/issues) before you post your own issue. Also it is good to check if there is a [newer release](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/releases) already available. Also take a look at daily built [developer images](https://github.com/manjaro-pinephone/phosh-dev/releases)! If you had bought your **Pinephone** with **Manjaro OS**, then you start with the [Factory Image (20201018)](https://osdn.net/projects/manjaro-arm/storage/pinephone/phosh/factory/).

![](https://gitlab.manjaro.org/manjaro-arm/issues/pinephone/phosh/-/raw/master/Emc8NO8XcAEhd0V.jpg)

Remember that the given software is still in heavy development and currently in **Beta** state. Not all features are fully functional yet.
You are welcome to help out and make the Linux Phone a working thing!
